#!/bin/bash

#  BTCR.sh
#  CodingKata
#
#  Created by Darren Lai on 2/14/19.
#  Copyright © 2019 kinwah.lai. All rights reserved.

. env.sh

ARG1=${1:-$DEFAULT_TEST}

echo "[BTCR] >> Run tests in $ARG1"
xcodebuild test-without-building -workspace "$WORKSPACE" -scheme "$SCHEME" -only-testing:"$ARG1" 2> /dev/null | xcpretty && exit ${PIPESTATUS[0]}
