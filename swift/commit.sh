#!/bin/bash

#  BTCR.sh
#  CodingKata
#
#  Created by Darren Lai on 2/14/19.
#  Copyright © 2019 kinwah.lai. All rights reserved.

. env.sh

echo "[BTCR] >> Commit source after test passed"
git commit -am "$COMMIT_MSG"
