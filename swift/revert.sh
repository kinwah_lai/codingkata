#!/bin/bash

#  BTCR.sh
#  CodingKata
#
#  Created by Darren Lai on 2/14/19.
#  Copyright © 2019 kinwah.lai. All rights reserved.

. env.sh

echo "[BTCR] >> Revert source in $SRC_DIR"
git checkout -- "$SRC_DIR"
