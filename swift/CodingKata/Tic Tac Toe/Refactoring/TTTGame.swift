//
//  TTTGame.swift
//  CodingKata
//
//  Created by Darren Lai on 5/28/19.
//  Copyright © 2019 kinwah.lai. All rights reserved.
//

import Foundation

enum TTTGameError: Error {
    case invalidFirstPlayer
    case invalidNextPlayer
    case invalidPosition
}

class T3Tile {
    let x: Int
    let y: Int
    var symbol: String
    
    init(_ symbol: String, _ x: Int, _ y: Int) {
        self.x = x
        self.y = y
        self.symbol = symbol
    }
}

class TTTBoard {
    private var plays: [T3Tile] = []
    
    init() {
        for x in 0..<3 {
            for y in 0..<3 {
                let tile = T3Tile("", x, y)
                plays.append(tile)
            }
        }
    }
    
    func tileAt(_ x: Int, _ y: Int) -> T3Tile? {
        return plays.first(where: { $0.x == x && $0.y == y })
    }
    
    func addTileAt(_ symbol: String, _ x: Int, _ y: Int) {
        if let tile = tileAt(x, y) {
            tile.symbol = symbol
        }
    }
}

class TTTGame {
    private var _lastSymbol: String = ""
    private var _board: TTTBoard = TTTBoard()
    var winner: String {
        //if the positions in first row are taken
        if let r1c1 = _board.tileAt(0, 0), let r1c2 = _board.tileAt(0, 1), let r1c3 = _board.tileAt(0, 2),
            !r1c1.symbol.isEmpty, !r1c2.symbol.isEmpty, !r1c3.symbol.isEmpty {
            //if first row is full with same symbol
            if r1c1.symbol == r1c2.symbol, r1c3.symbol == r1c2.symbol {
                return r1c1.symbol
            }
        }
        //if the positions in first row are taken
        if let r1c1 = _board.tileAt(1, 0), let r1c2 = _board.tileAt(1, 1), let r1c3 = _board.tileAt(1, 2),
            !r1c1.symbol.isEmpty, !r1c2.symbol.isEmpty, !r1c3.symbol.isEmpty {
            //if middle row is full with same symbol
            if r1c1.symbol == r1c2.symbol, r1c3.symbol == r1c2.symbol {
                return r1c1.symbol
            }
        }
        //if the positions in first row are taken
        if let r1c1 = _board.tileAt(2, 0), let r1c2 = _board.tileAt(2, 1), let r1c3 = _board.tileAt(2, 2),
            !r1c1.symbol.isEmpty, !r1c2.symbol.isEmpty, !r1c3.symbol.isEmpty {
            //if middle row is full with same symbol
            if r1c1.symbol == r1c2.symbol, r1c3.symbol == r1c2.symbol {
                return r1c1.symbol
            }
        }
        return ""
    }
    
    func play(_ symbol: String, _ x: Int, _ y: Int) throws {
        //if first move, if player is X
        if _lastSymbol.isEmpty, symbol == "O" {
            throw TTTGameError.invalidFirstPlayer
        }
        //if not first move but player repeated
        if _lastSymbol == symbol {
            throw TTTGameError.invalidNextPlayer
        }
        //if not first move but play on an already played tile
        if let tile = _board.tileAt(x,y), tile.symbol != "" {
            throw TTTGameError.invalidPosition
        }
        
        // update game state
        _lastSymbol = symbol
        _board.addTileAt(symbol, x, y)
    }
}
