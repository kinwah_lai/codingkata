//
//  DescendingOrder.spec.swift
//  CodingKataTests
//
//  Created by Darren Lai on 8/17/18.
//  Copyright © 2018 kinwah.lai. All rights reserved.
//

import Quick
import Nimble

@testable import CodingKata

/*
 Your task is to make a function that can take any non-negative integer as a argument and return it with its digits in descending order. Essentially, rearrange the digits to create the highest possible number.
 Examples:
 
 Input: 21445 Output: 54421
 
 Input: 145263 Output: 654321
 
 Input: 1254859723 Output: 9875543221
 */

class DescendingOrder_spec: QuickSpec {
    override func spec() {
        describe("function to rearrange number in descending order") {
            it("will failed") {
                expect(true).to(beFalse())
            }
        }
    }
}

