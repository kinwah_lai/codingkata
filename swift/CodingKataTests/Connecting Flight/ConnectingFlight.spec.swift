//
//  ConnectingFlight.spec.swift
//  CodingKataTests
//
//  Created by Darren Lai on 2/27/19.
//  Copyright © 2019 kinwah.lai. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import CodingKata

/*
 given data
 1. JFK, LAX
 2. SFO, HOU
 3. HOU, JFK
 
 Output
 SFO -> HOU -> JFK -> LAX
 */

class ConnectingFlight_spec: QuickSpec {
    override func spec() {
        describe("A utility that can make a connecting flight with the given array of data") {
            it("single destination") {
                expect(true).to(beFalse())
            }
        }
    }
}
