//
//  TicTacToe.spec.swift
//  CodingKataTests
//
//  Created by Darren Lai on 2/8/19.
//  Copyright © 2019 kinwah.lai. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import CodingKata

/*
 Tic Tac Toe rules
 1. X always goes first.
 2. Players alternate placing X’s and O’s on the board.
 3. Players cannot play on a played position.
 4. A player with three X’s or O’s in a row (horizontally, vertically, diagonally) wins.
 5 .If all nine squares are filled and neither player achieves three in a row, the game is a draw.
 */

class TicTacToe_spec: QuickSpec {
    override func spec() {
        describe("Tic Tac Toe game") {
            it("empty board") {
                expect(true).to(beFalse())
            }
        }
    }
}

