//
//  TTTGame.spec.swift
//  CodingKataTests
//
//  Created by Darren Lai on 5/28/19.
//  Copyright © 2019 kinwah.lai. All rights reserved.
//

import XCTest
import Quick
import Nimble

@testable import CodingKata

// Translated from https://github.com/AgileTechPraxis/CodeSmells

class TTTGame_spec: QuickSpec {
    private var game: TTTGame!
    
    override func spec() {
        beforeEach {
            self.game = TTTGame()
        }
        describe("Tic Tac Toe game") {
            it("Player O cannot play first") {
                expect { try self.game.play("O", 0, 0) }.to(throwError())
            }

            it("Player X cannot play twice in a row") {
                try! self.game.play("X", 0, 0)
                expect { try self.game.play("X", 1, 0) }.to(throwError())
            }

            it("Player cannot play in Last Played position") {
                try! self.game.play("X", 0, 0)
                expect { try self.game.play("O", 0, 0) }.to(throwError())
            }

            it("Player cannot play in Any Played position") {
                try! self.game.play("X", 0, 0)
                try! self.game.play("O", 1, 0)
                expect { try self.game.play("X", 0, 0) }.to(throwError())
            }

            it("Declare player X as winner if three in top row") {
                try! self.game.play("X", 0, 0)
                try! self.game.play("O", 1, 0)
                try! self.game.play("X", 0, 1)
                try! self.game.play("O", 1, 1)
                try! self.game.play("X", 0, 2)
                expect(self.game.winner).to(equal("X"))
            }

            it("Declare player O as winner if three in middle row") {
                try! self.game.play("X", 0, 0)
                try! self.game.play("O", 1, 0)
                try! self.game.play("X", 2, 0)
                try! self.game.play("O", 1, 1)
                try! self.game.play("X", 2, 1)
                try! self.game.play("O", 1, 2)
                expect(self.game.winner).to(equal("O"))
            }

            it("Declare player X as winner if three in bottom row") {
                try! self.game.play("X", 2, 0)
                try! self.game.play("O", 0, 0)
                try! self.game.play("X", 2, 1)
                try! self.game.play("O", 0, 1)
                try! self.game.play("X", 2, 2)
                expect(self.game.winner).to(equal("X"))
            }

            it("Declare player O as winner if three in bottom row") {
                try! self.game.play("X", 0, 0)
                try! self.game.play("O", 2, 0)
                try! self.game.play("X", 1, 0)
                try! self.game.play("O", 2, 1)
                try! self.game.play("X", 1, 1)
                try! self.game.play("O", 2, 2)
                expect(self.game.winner).to(equal("O"))
            }
        }
    }
}
