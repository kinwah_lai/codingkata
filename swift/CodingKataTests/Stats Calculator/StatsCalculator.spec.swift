//
//  StatsCalculator.spec.swift
//  CodingKataTests
//
//  Created by Darren Lai on 1/29/19.
//  Copyright © 2019 kinwah.lai. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import CodingKata

/*
 Stats calculator by cyber-dojo
 
 Your task is to process a sequence of integet numbers to determine the follow ing statistic:
 * minimum value
 * maximum value
 * number of elements in the sequence
 * average value
 
 Example: [6, 9, 15, -2, 92, 11]
 min = -2
 max = 92
 count = 6
 average = 21.8333
 */

class StatsCalculator_spec: QuickSpec {
    override func spec() {
        describe("Determine statistic for a sequence of interger numbers") {
            it("TO BE IMPLEMENTED") {
                expect(true).to(beFalse())
            }
        }
    }
}
