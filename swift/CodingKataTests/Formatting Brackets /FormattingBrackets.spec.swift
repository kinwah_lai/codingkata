//
//  FormattingBrackets.spec.swift
//  CodingKataTests
//
//  Created by Darren Lai on 3/13/19.
//  Copyright © 2019 kinwah.lai. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import CodingKata

/*
 1. "{}"     => True
 2. "{"      => False
 3. "[]"     => True
 4. "{[]}"   => True
 5. "{[}"    => False
 6. "{[][]}" => True
 7. "{[{}]}" => true
 */
class FormattingBrackets_spec: QuickSpec {
    override func spec() {
        describe("Check if the brackets in string is balance") {
            it("string with {} is a formatted brackets") {
                let formatter = FormattingBrackets()
                expect(formatter.isFormatted("{}")).to(beTrue())
            }
        }
    }
}
