//
//  ArrayScores.spec.swift
//  CodingKataTests
//
//  Created by Darren Lai on 1/10/19.
//  Copyright © 2019 kinwah.lai. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import CodingKata

/*
 This kata was introduced by Steve from Seattle codecrafter meetup
 Given that there are 2 arrays of Integer and element count for the 2 arrays are always the same.
 
 You are required to calculate the score of the arrays with these rules
 A > B , A scores 1
 B > A , B scores 1
 A == B , no score for both
 
 Example:
 arrayA [4, 4, 1, 9]
 arrayB [3, 6, 1, 5]
 
 result = "2 1"
 */

class ArrayScores_spec: QuickSpec {
    override func spec() {
        describe("Calculate score for a given 2 integer arrays") {
            it("a '0 0' if empty array passed in") {
                expect(true).to(beFalse())
            }
        }
    }
}
