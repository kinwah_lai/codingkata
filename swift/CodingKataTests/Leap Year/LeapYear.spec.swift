//
//  LeapYear.spec.swift
//  CodingKataTests
//
//  Created by Darren Lai on 12/12/18.
//  Copyright © 2018 kinwah.lai. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import CodingKata

/*
 In this kata you should simply determine, whether a given year is a leap year or not. In case you don't know the rules, here they are:
 
 years divisible by 4 are leap years
 but years divisible by 100 are no leap years
 but years divisible by 400 are leap years
 
 Additional Notes:
 
 Only valid years (positive integers) will be tested, so you don't have to validate them
 
 Examples can be found in the test fixture.

 */

class LeapYear_spec: QuickSpec {
    override func spec() {
        describe("A function that can determine if a year input is a leap year or not") {
            it("2001 is not a leap year") {
                expect(true).to(beFalse())
            }
        }
    }
}
