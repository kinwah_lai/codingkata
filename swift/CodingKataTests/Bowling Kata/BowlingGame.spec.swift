//
//  BowlingGame.spec.swift
//  CodingKata
//
//  Created by Darren Lai on 5/25/18.
//Copyright © 2018 kinwah.lai. All rights reserved.
//

import Quick
import Nimble

// a game has 10 frames
// each frame has 10 pins and 2 roll to knock down pins, only 10th frames can have 3rd roll if it is a spare/strike
// a spare is user hit 10pins in 2 roll, then first roll of the next frame will be the bonus, eg: 10 + 4
// a strike is user hit 10pins in 1 roll, then the subsequent 2 roll will be the bonus, eg: 10 + 3 + 4
// score will be calculated at game ends.

class BowlingGame_spec: QuickSpec {
    override func spec() {
        describe("bowling game score tracking") {
            it("failing test") {
                expect(false).to(beTrue())
            }
        }
    }
}
