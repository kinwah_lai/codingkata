#!/bin/bash

#  BTCR.sh
#  CodingKata
#
#  Created by Darren Lai on 2/14/19.
#  Copyright © 2019 kinwah.lai. All rights reserved.

. env.sh

echo "[BTCR] >> Build scheme $SCHEME in $WORKSPACE"
xcodebuild build-for-testing -workspace "$WORKSPACE" -scheme "$SCHEME" | xcpretty -tc && exit ${PIPESTATUS[0]}
