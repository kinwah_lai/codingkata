#!/bin/bash

#  BTCR.sh
#  CodingKata
#
#  Created by Darren Lai on 2/14/19.
#  Copyright © 2019 kinwah.lai. All rights reserved.

ARG1=${1:-$DEFAULT_TEST}

./buildIt.sh && (./runTests.sh $ARG1 && ./commit.sh || ./revert.sh)
