# Coding Kata for Javascript

## Installation
* Git clone the repo
* run npm install

## Run test
* to run all test - npm test
* to run single test - npm test -- --grep Fizz

## Available kata
* FizzBuzz
* 100 doors